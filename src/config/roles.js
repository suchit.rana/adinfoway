const allRoles = {
  user: [],
  admin: ['getUsers', 'manageUsers'],
};

const roles = {
  ADMIN: 1,
  USER: 2,
  SUPER_ADMIN: 3,
};

const roleRights = new Map(Object.entries(allRoles));

module.exports = {
  roles,
  roleRights,
};
